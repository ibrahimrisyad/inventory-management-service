# Angular Inventory Management System

This is a simple Inventory Management System built with Angular. It helps streamline inventory management processes for a small retail business.

## Features

- View and manage inventory items
- Add new inventory items with fields for item name, description, price, and quantity in stock
- Edit and delete inventory items
- Set stock alerts
- View sales history of inventory items
- Associate inventory items with suppliers
- User roles and permissions (Admin and Staff)

## Getting Started

Follow these instructions to get a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Node.js (https://nodejs.org)
- Angular CLI (https://angular.io/cli)

### Installing

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/ibrahimrisyad/inventory-management-service.git

2.  Navigate into the project directory:
    cd angular-inventory-management-system

3. Install Dependencies
    npm install

4. Run The Application:
    ng serve
    Open your browser and navigate to http://localhost:4200/ to view the application.

5. Login Application :
    for admin :                                 for staff :
      username : admin                            username : staff
      pass     : admin                            pass     : staff

      

Built With
Angular - Frontend framework
Angular Material - UI component library
RxJS - Reactive programming library
SCSS - CSS preprocessor
