import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { UserRole } from '../auth/user-rule.enum';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserRoleSubject: BehaviorSubject<UserRole | null> = new BehaviorSubject<UserRole | null>(null);

  private users = [
    { username: 'admin', password: 'admin', role: UserRole.Admin },
    { username: 'staff', password: 'staff', role: UserRole.Staff }
  ];

  constructor() {}

  login(username: string, password: string): Observable<boolean> {
    const user = this.users.find(u => u.username === username && u.password === password);
    if (user) {
      this.setCurrentUserRole(user.role);
      return of(true);
    } else {
      return of(false);
    }
  }

  logout(): Observable<boolean> {
    this.setCurrentUserRole(null);
    return of(true);
  }

  getCurrentUserRole(): Observable<UserRole | null> {
    return this.currentUserRoleSubject.asObservable();
  }

  setCurrentUserRole(role: UserRole | null): void {
    this.currentUserRoleSubject.next(role);
  }

  isUserAdmin(): Observable<boolean> {
    return this.getCurrentUserRole().pipe(
      tap(role => console.log('User role:', role)),
      map(role => role === UserRole.Admin)
    );
  }

  isUserStaff(): Observable<boolean> {
    return this.getCurrentUserRole().pipe(
      map(role => role === UserRole.Staff)
    );
  }
}
