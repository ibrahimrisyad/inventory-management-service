import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { InventoryItem, Sale } from '../models/inventory.model';
import { Supplier } from '../models/supplier.model';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {
  private inventory: BehaviorSubject<InventoryItem[]> = new BehaviorSubject<InventoryItem[]>([]);
  private thresholdMap: Map<number, number> = new Map<number, number>();
  private suppliers: BehaviorSubject<Supplier[]> = new BehaviorSubject<Supplier[]>([]);

  constructor() {}

  getInventory(): Observable<InventoryItem[]> {
    return this.inventory.asObservable();
  }

  getThreshold(itemId: number): number | undefined {
    return this.thresholdMap.get(itemId);
  }

  setThreshold(itemId: number, threshold: number): void {
    this.thresholdMap.set(itemId, threshold);
  }

  addInventoryItem(item: InventoryItem): void {
    const currentInventory = [...this.inventory.value]; // Make a copy of the array
    currentInventory.push(item);
    this.inventory.next(currentInventory);
  }

  updateInventoryItem(item: InventoryItem): void {
    const currentInventory = [...this.inventory.value]; // Make a copy of the array
    const index = currentInventory.findIndex(i => i.id === item.id);
    if (index !== -1) {
      currentInventory[index] = item;
      this.inventory.next(currentInventory);
    } else {
      console.error(`Item with id ${item.id} not found.`);
    }
  }

  deleteInventoryItem(id: number): void {
    this.thresholdMap.delete(id);
    const currentInventory = this.inventory.value.filter(item => item.id !== id);
    this.inventory.next(currentInventory);
  }

  recordSale(itemId: number, quantitySold: number): void {
    const currentInventory = [...this.inventory.value]; // Make a copy of the array
    const itemIndex = currentInventory.findIndex(item => item.id === itemId);

    if (itemIndex !== -1) {
      const item = currentInventory[itemIndex];
      const totalRevenue = quantitySold * item.price;

      const sale = new Sale(new Date(), quantitySold, totalRevenue);
      item.salesHistory.push(sale);

      item.quantityInStock -= quantitySold;

      this.inventory.next(currentInventory);
    } else {
      console.error(`Item with id ${itemId} not found.`);
    }
  }

  getSuppliers(): Observable<Supplier[]> {
    return this.suppliers.asObservable();
  }

  addSupplier(supplier: Supplier): void {
    const currentSuppliers = [...this.suppliers.value]; // Make a copy of the array
    currentSuppliers.push(supplier);
    this.suppliers.next(currentSuppliers);
  }

  associateItemWithSupplier(itemId: number, supplierId: number): void {
    const currentInventory = [...this.inventory.value]; // Make a copy of the array
    const itemIndex = currentInventory.findIndex(item => item.id === itemId);

    if (itemIndex !== -1) {
      currentInventory[itemIndex].supplierId = supplierId;
      this.inventory.next(currentInventory);
    } else {
      console.error(`Item with id ${itemId} not found.`);
    }
  }
}
