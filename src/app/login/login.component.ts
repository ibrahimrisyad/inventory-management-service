import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  username: string = '';
  password: string = '';
  loginError: string | null = null;

  constructor(private authService: AuthService, private router: Router) {}

  login(): void {
    this.authService.login(this.username, this.password).subscribe((loggedIn: boolean) => {
      if (loggedIn) {
        // Redirect to the appropriate page based on user role
        this.authService.getCurrentUserRole().subscribe(role => {
          if (role === 'admin') {
            this.router.navigate(['/inventory']);
          } else if (role === 'staff') {
            this.router.navigate(['/inventory']);
          }
        });
      } else {
        // Handle login failure
        this.loginError = 'Invalid username or password. Please try again.';
      }
    });
  }
}
