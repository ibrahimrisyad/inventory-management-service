// staff.guard.ts
import { Injectable, inject } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivateFn } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
class Staff {
  isAdmin!: boolean;
  isStaff!: boolean;
  constructor(private authService: AuthService, private router: Router) {}
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean  {
    this.authService.isUserAdmin().subscribe(isAdmin => {
      this.isAdmin = isAdmin;
    });
    this.authService.isUserStaff().subscribe(isStaff => {
      this.isStaff = isStaff
    });
    return this.isAdmin || this.isStaff;
  }
}

export const StaffGuard: CanActivateFn = (next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean => {
  return inject(Staff).canActivate(next, state);
}
