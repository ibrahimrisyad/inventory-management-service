import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';

import { InventoryService } from './services/inventory.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SupplierManagementComponent } from './components/supplier/supplier.component';
import { InventoryComponent } from './components/inventory/inventory.component';
import { AddEditInventoryComponent } from './components/add-edit-inventory/add-edit-inventory.component';
import { AuthGuard } from './auth/admin.guard';
import { StaffGuard } from './auth/staff.guard';

const appRoutes: Routes = [
  { path: 'inventory', component: InventoryComponent, canActivate: [StaffGuard] },
  { path: 'add', component: AddEditInventoryComponent, canActivate: [AuthGuard] },
  { path: 'supplier', component: SupplierManagementComponent, canActivate: [AuthGuard] },
  { path: 'edit/:id', component: AddEditInventoryComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    InventoryComponent,
    AddEditInventoryComponent,
    SupplierManagementComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatToolbarModule,
    RouterModule.forRoot(appRoutes, { useHash: true })
  ],
  providers: [InventoryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
