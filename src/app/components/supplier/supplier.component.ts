import { Component, OnInit } from '@angular/core';
import { InventoryService } from '../../services/inventory.service';
import { Supplier } from '../../models/supplier.model';

@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.scss']
})
export class SupplierManagementComponent implements OnInit {
  suppliers: Supplier[] = [];

  constructor(private inventoryService: InventoryService) {}

  ngOnInit(): void {
    this.inventoryService.getSuppliers().subscribe((suppliers) => {
      this.suppliers = suppliers;
    });
  }

  addSupplier(name: string, contactInfo: string): void {
    const newSupplier = new Supplier(this.generateSupplierId(), name, contactInfo);
    this.inventoryService.addSupplier(newSupplier);
  }

  private generateSupplierId(): number {
    return this.suppliers.length + 1;
  }
}
