import { Component, OnInit } from '@angular/core';
import { InventoryService } from '../../services/inventory.service';
import { InventoryItem } from '../../models/inventory.model';
import { Supplier } from '../../models/supplier.model';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {
  displayedColumns: string[] = ['itemName', 'description', 'price', 'quantityInStock', 'actions', 'salesHistory', 'supplierId'];
  dataSource: InventoryItem[] = [];
  suppliers: Supplier[] = [];


  constructor(private inventoryService: InventoryService) {}

  ngOnInit(): void {
    this.inventoryService.getInventory().subscribe((inventory) => {
      this.dataSource = inventory;
    });
    this.inventoryService.getSuppliers().subscribe((suppliers) => {
      this.suppliers = suppliers;
    });
  }

  deleteItem(id: number): void {
    this.inventoryService.deleteInventoryItem(id);
  }

  isLowStock(item: InventoryItem): boolean {
    const threshold = this.inventoryService.getThreshold(item.id);
    return threshold !== undefined && item.quantityInStock <= threshold;
  }

  sellItem(item: InventoryItem): void {
    const quantitySold = 1;
    if(quantitySold <= item.quantityInStock){
      this.inventoryService.recordSale(item.id, quantitySold);
    }
  }
}
