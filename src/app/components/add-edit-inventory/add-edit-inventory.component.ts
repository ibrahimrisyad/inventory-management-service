import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InventoryService } from '../../services/inventory.service';
import { InventoryItem } from '../../models/inventory.model';
import { Supplier } from '../../models/supplier.model';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-add-edit-inventory',
  templateUrl: './add-edit-inventory.component.html',
  styleUrls: ['./add-edit-inventory.component.scss']
})
export class AddEditInventoryComponent implements OnInit {
  inventoryForm!: FormGroup;
  isEditMode = false;
  itemId!: number;
  suppliers: Supplier[] = [];
  isAdmin!: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private inventoryService: InventoryService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.initializeForm();
    this.route.params.subscribe((params) => {
      if (params['id']) {
        this.isEditMode = true;
        this.itemId = +params['id'];
        this.populateForm(this.itemId);
      }
    });
    this.inventoryService.getSuppliers().subscribe((suppliers) => {
      this.suppliers = suppliers;
    });
  }
  private initializeForm(): void {
    this.inventoryForm = this.formBuilder.group({
      itemName: ['', Validators.required],
      description: ['', Validators.required],
      price: ['', [Validators.required, Validators.min(0)]],
      quantityInStock: ['', [Validators.required, Validators.min(0)]],
      threshold: ['', [Validators.required, Validators.min(0)]],
      supplierId: ['', Validators.required]
    });
  }

  private populateForm(itemId: number): void {
    this.inventoryService.getInventory().subscribe((inventory) => {
      const selectedItem = inventory.find((item) => item.id === itemId);

      if (selectedItem) {
        this.inventoryForm.setValue({
          itemName: selectedItem.itemName,
          description: selectedItem.description,
          price: selectedItem.price,
          quantityInStock: selectedItem.quantityInStock,
          threshold: this.inventoryService.getThreshold(itemId) || 0,
          supplierId: selectedItem.supplierId,
        });
      } else {
        console.error(`Item with id ${itemId} not found.`);
      }
    });
  }

  onSubmit(): void {
    const formValue = this.inventoryForm.value;
    const newItem = new InventoryItem(
      this.isEditMode ? this.itemId : this.generateItemId(),
      formValue.itemName,
      formValue.description,
      formValue.price,
      formValue.quantityInStock,
      formValue.supplierId
    );

    this.inventoryService.setThreshold(newItem.id, formValue.threshold);

    if (this.isEditMode) {
      this.inventoryService.updateInventoryItem(newItem);
    } else {
      this.inventoryService.addInventoryItem(newItem);
    }

    this.router.navigate(['/inventory']);
  }

  private generateItemId(): number {
    return Date.now();
  }
}
