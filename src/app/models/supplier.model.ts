export class Supplier {
  constructor(
    public id: number,
    public name: string,
    public contactInfo: string,
    public suppliedItems: number[] = []
  ) {}
}
