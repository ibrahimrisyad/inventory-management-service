export class InventoryItem {
  constructor(
    public id: number,
    public itemName: string,
    public description: string,
    public price: number,
    public quantityInStock: number,
    public supplierId: number | null = null,
    public salesHistory: Sale[] = [],
  ) {}
}

export class Sale {
  constructor(
    public date: Date,
    public quantitySold: number,
    public totalRevenue: number
  ) {}
}
